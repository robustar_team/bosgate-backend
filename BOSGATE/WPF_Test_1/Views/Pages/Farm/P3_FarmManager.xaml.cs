﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P3_DeviceManager.xaml
    /// </summary>
    public partial class P3_FarmManager : Page
    {
        List<Farm> Farms = new List<Farm>();
        DatabaseController svDB = new DatabaseController(1);
        Dictionary<string, int> DevCount_As_FarmID = new Dictionary<string, int>();
        Dictionary<string, int> CowCount_As_FarmID = new Dictionary<string, int>();
        public P3_FarmManager()
        {
            InitializeComponent();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            Farms = new List<Farm>();
            Farms = svDB.Farm_SelectAll();
            DevCount_As_FarmID = svDB.Farm_DeviceDict_As_farmid();
            CowCount_As_FarmID = svDB.Farm_CowDict_As_farmid();
            foreach (Farm fa in Farms)
                {
                    try
                    {
                        fa.NoDevice = DevCount_As_FarmID[fa.FarmID];
                    }
                    catch
                    { // if there is no key for farmid 
                      //--> no device in that farm
                        fa.NoDevice = 0;
                    }

                    try
                    {
                        fa.NoCow = CowCount_As_FarmID[fa.FarmID];
                    }
                    catch
                    {// if there is no key for farmid 
                     //--> no cow in that farm
                    fa.NoCow = 0;
                    }
            }
            dgFarm.ItemsSource = Farms;
        }

        private void dgDevice_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            try
            {
                if (e.Row.GetIndex() % 2 == 0)
                {
                    e.Row.Background = new SolidColorBrush(Colors.White);
                    e.Row.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    e.Row.Background = new SolidColorBrush(Colors.DimGray);
                    e.Row.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }
        }

        private void TBox_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgFarm.ItemsSource);
            cv.Filter = o => {
                /* change to get data row value */
                Farm p = o as Farm;
                return (p.FarmID.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }

        private void But_AddFarm_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new P32_FarmAdd(""), UriKind.Relative);
        }

        private void dgFarm_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                Farm choosenFarm = (Farm)dgFarm.SelectedItem;
                this.NavigationService.Navigate(new P31_FarmInfo(choosenFarm.FarmID), UriKind.Relative);
            }
            catch { }
        }
    }
}
