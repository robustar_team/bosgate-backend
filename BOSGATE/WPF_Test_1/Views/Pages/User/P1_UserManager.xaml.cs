﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for P1_UserManager.xaml
    /// </summary>
    public partial class P1_UserManager : Page
    {
        List<User> users = new List<User>();
        WebApiConnector webConnect = new WebApiConnector();
        DatabaseController svDB = new DatabaseController(1);

        public P1_UserManager()
        {
            InitializeComponent();
            dgUser.Items.Clear();
        }

        private void Page_Loaded(object sender, RoutedEventArgs e)
        {
            users = new List<User>();
            users = svDB.User_SelectAll();
            dgUser.ItemsSource = users;
        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {

        }

        // Điều hướng tới trang thông tin User
        private void dgUser_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                User choosenOne = (User)dgUser.SelectedItem;
                this.NavigationService.Navigate(new P11_UserPage(choosenOne.Id), UriKind.Relative);
            }
            catch { }
        }

        // khởi tạo Template cho Data Grid
        public void dgUser_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            try
            {
                if (e.Row.GetIndex() % 2 == 0)
                {
                    e.Row.Background = new SolidColorBrush(Colors.White);
                    e.Row.Foreground = new SolidColorBrush(Colors.Black);
                }
                else
                {
                    e.Row.Background = new SolidColorBrush(Colors.DimGray);
                    e.Row.Foreground = new SolidColorBrush(Colors.White);
                }
            }
            catch
            {
            }
        }

        private void TBox_Search_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBoxName = (TextBox)sender;
            string filterText = textBoxName.Text;

            ICollectionView cv = CollectionViewSource.GetDefaultView(dgUser.ItemsSource);
            cv.Filter = o => {
                /* change to get data row value */
                User p = o as User;
                return (p.Username.ToUpper().Contains(filterText.ToUpper()));
                /* end change to get data row value */
            };
        }
    }
}
