﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for Page1.xaml
    /// </summary>
    /// 

    public partial class MainPage : Page
    {
        P1_UserManager userManager = new P1_UserManager();
        P2_CustomerManager cusManger = new P2_CustomerManager();
        P3_FarmManager devManager = new P3_FarmManager();
        P4_DeviceCowManager cowManager = new P4_DeviceCowManager();
        //P4_Standard standard = new P4_Standard();
        //P5_Report report = new P5_Report();
        //P6_Notification notification = new P6_Notification();

        public MainPage()
        {
            InitializeComponent();
        }

        private void but_User_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(userManager, UriKind.Relative);
        }

        private void but_Customer_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(cusManger, UriKind.Relative);
        }

        private void but_Device_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(devManager, UriKind.Relative);
        }

        private void but_Cow_Click(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(cowManager, UriKind.Relative);
        }

        private void but_Standard_Click(object sender, RoutedEventArgs e)
        {

        }

        private void but_Notification_Click(object sender, RoutedEventArgs e)
        {

        }

    }
}
