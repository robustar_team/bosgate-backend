﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Navigation;

namespace BOSGATE
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : NavigationWindow
    {
        DatabaseController lcData = new DatabaseController(0);
        DatabaseController svData = new DatabaseController(1);

        public MainWindow()
        {
            InitializeComponent();
            List<User> users = lcData.User_SelectAll();
            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(DailyCowUpdate);
            dispatcherTimer.Interval = new TimeSpan(0,0,20);
            dispatcherTimer.Start();

        }

        private void mainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            LogInPage loginPage = new LogInPage();
            this.NavigationService.Navigate(loginPage);
        }

        private void DailyCowUpdate(object sender, EventArgs e)
        {
            // update Daily Cow Table for the cycle of 20s

        }
    }
}
