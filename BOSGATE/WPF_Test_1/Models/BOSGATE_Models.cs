﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BOSGATE
{
    public class User
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Password_hash { get; set; }
        public string Birthday { get; set; }
        public int Position { get; set; }
        public string PositionName { get; set; }
        public int EmpID { get; set; }
        public string DirectManager { get; set; }
        public string Api_key { get; set; }
        public int Status { get; set; }
        public string Created_at { get; set; }

        public User()
        {

        }
        
        public User(int id, string username, string email, string birthday, int empid, int position, string createdat)
        {
            Id = id;
            Username = username;
            Email = email;
            Birthday = birthday;
            Position = position;
            EmpID = empid;
            Created_at = createdat;

            switch (Position)
            {
                case 0:
                    PositionName = "Quản lí";
                    break;
                case 1:
                    PositionName = "Nhân viên";
                    break;
                case 2:
                    PositionName = "Kĩ thuật viên";
                    break;
                default:
                    PositionName = "Nhân Viên";
                    break;
            }       
        }
    }
    public class Root_User
    {
        public bool error { get; set; }
        public List<User> users { get; set; }
    }

    public class Customer
    {
        public int ID { get; set; }
        public string CustomerID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
        public string Created_at { get; set; }
        public string Password_hash { get; set; }

        public Customer()
        {

        }
        public Customer(string custid, string name, string email, string phone, string passhash)
        {
            CustomerID = custid;
            Name = name;
            Email = email;
            Phone = phone;
            Status = 0;
            Created_at = DateTime.Today.ToLongDateString();
        }
    }
    public class Root_Customer
    {
        public bool error { get; set; }
        public List<Customer> customers { get; set; }
    }

    public class Farm
    {
        public int Id { get; set; }
        public string FarmID { get; set; }
        public string CustomerID { get; set; }
        public string FarmName { get; set; }
        public string Location { get; set; }
        public double AreaSize { get; set; }
        public int NoDevice { get; set; }
        public int NoCow { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
    }

    public class Device
    {
        public int Id { get; set; }
        public string DeviceID { get; set; }
        public string FarmID { get; set; }
        public string HWVer { get; set; }
        public string SWVer { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }
        public string Created_at { get; set; }
        public string Sim_SN { get; set; }
        public string Sim_ExpiryDate { get; set; }
        public string Sim_Balance { get; set; }
        public string BatteryStatus { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }

        public Device() { }

        public Device(string devID, string farmid, string hwver, string swver, string address, string name)
        {
            DeviceID = devID;
            FarmID = farmid;
            HWVer = hwver;
            SWVer = swver;
            Address = address;
            Name = name;
        }
    }
    public class Root_Device
    {
        public bool error { get; set; }
        public List<Device> devices { get; set; }
    }

    public class DailyDevice
    {
        public string DeviceID { get; set; }
        public string Date { get; set; }
        public string Temperature { get; set; }
        public string Humidity { get; set; }
        public double Food_Day { get; set; }
    }
    public class Root_DailyDevice
    {
        public bool error { get; set; }
        public List<DailyDevice> dailydevices { get; set; }
    }


    public class Cow
    {
        public int Id { get; set; }
        public string CowID { get; set; }
        public string FarmID { get; set; }
        public string TagID_Left { get; set; }
        public string TagID_Right { get; set; }
        public string Date_In { get; set; }
        public string Date_Out { get; set; }
        public double Weight_Org { get; set; }
        public string Kind_Info { get; set; }
        public int Day_Old { get; set; }
        public double Food_Digest { get; set; }
        public double Eff_Breeding { get; set; }
        public int Status { get; set; }
        public string StatusName { get; set; }
    }
    public class Root_Cow
    {
        public bool error { get; set; }
        public List<Device> cows { get; set; }
    }

    public class DailyCow
    {
        public string CowID { get; set; }
        public string Date { get; set; }
        public double Weight { get; set; }
    }
    public class Root_DailyCow
    {
        public bool error { get; set; }
        public List<DailyCow> dailycow { get; set; }
    }


    public class Cost
    {
        public int CostID { get; set; }
        public string Update_Date { get; set; }
        public double Stand_Eff_Ratio { get; set; }
        public int Cost_Bogaca { get; set; }
        public int Cost_Meat { get; set; }
    }
    public class Root_Cost
    {
        public bool error { get; set; }
        public List<Cost> costs { get; set; }
    }


    public class Standard
    {
        public int id { get; set; }
        public int day_old { get; set; }
        public double weight_gain { get; set; }
        public int weight_std { get; set; }

        public Standard()
        {

        }
    }
    public class Root_Standard
    {
        public bool error { get; set; }
        public List<Standard> weight_sts { get; set; }
    }

}
